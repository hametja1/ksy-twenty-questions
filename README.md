# KSY - Pepper twenty questions

An application for the robot Pepper, which alows Pepper to play game The 20 questions. Bot is trained with an artificial neural network to play a simplified version of 20 questions.
Uses a "zoo" dataset as guess options for the user and targets for the bot, with animal features
forming the questions and answers.

## Instructions

The program runs on Python 3.5, required modules are in requirements.txt

There are two modes and four datasets which you can use to interact with the bot:

MODES:
1. 'play': load a previously trained and optimised network and run it in the Pepper. (needs setup ip addres of the Pepper robot in PEPPER_IP variable)
2. 'playPC' load a previously trained and optimised network and run it on PC.
2. 'crossvalidate': train a network for a dataset and then play with that network.

DATASETS:
1. 'micro': for very basic training. 5 animals + 5 questions, question limit = 4.
2. 'small': testing basic intelligence. 12 animals + 6 questions, question limit = 5.
3. 'medium': for testing smart question selection. 13 animals + 15 questions, question limit = 6.
4. 'big': the big kahuna... 58 animals + 28 questions, question limit = 13. 

To run the bot in either mode with a dataset:

python 20q.py <DATASET> <MODE> <IP_ADDRES>

e.g.
python 20q.py medium play 10.37.2.27
python 20q.py big playPC

Or you can run it in your IDE through __main__ method.

It is recommended that 'crossvalidate' mode only be used on datasets micro, small or medium (not big),
as training for 'big' takes 20-30 mins.

This project runs with [PepperController](https://github.com/incognite-lab/Pepper-Controller) project from Incognite lab.